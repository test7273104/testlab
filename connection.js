const mysql = require('mysql2');
const mysqlConnection = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"root",
    database:"crud"
});

var connection=mysqlConnection.connect((err)=>{
    if(err)
    throw err;
    else
    console.log("DB Connect successfully");
})

module.exports = mysqlConnection;
