const connection = require("./connection");

const express = require('express');
const bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());

app.get("/employees",(req,resp)=>{
    connection.query('select * from employee',(err,rows)=>{
        console.log('in get');
        if(err)
        console.log(err);
        else
        //console.log(rows);
        resp.send(rows);
    })
});

app.get("/employees/:id",(req,resp)=>{
    connection.query('select * from employee where id = ?',[req.params.id],(err,rows)=>{
        console.log('in get');
        if(err)
        console.log(err);
        else
        //console.log(rows);
        resp.send(rows);
    })
});

app.delete("/employees/:id",(req,resp)=>{
    connection.query('delete from employee where id = ?',[req.params.id],(err,rows)=>{
        console.log('in get');
        if(err)
        console.log(err);
        else
        //console.log(rows);
        resp.send(rows);
    })
});

app.post("/employees",(req,resp)=>{
    const emp = {
        name : req.body.name,
        salary : req.body.salary
    };
    console.log(emp);
    var sql = `insert into employee(name,salary) values('${emp.name}','${emp.salary}')`;
    connection.query(sql,(err,rows)=>{
        if(err){
            console.log(err);
        }
        else{
            console.log(rows);
        }
    })

});


app.put("/employees/:id",(req,res)=>{
    var emp = {
        name : req.body.name,
        salary : req.body.salary
    };

    var sql = `update from employee set name = '${emp.name}', salary = ${emp.salary} where id = ${req.params.id}`;
    connection.query(sql,(err,rows)=>{
        if(err){
            console.log(err);
        }
        else{
            console.log(rows);
        }
    });


});



app.listen(3000,()=>{
    console.log("server is runnning at port 3000");

});
